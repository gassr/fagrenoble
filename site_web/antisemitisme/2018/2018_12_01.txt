

================================
Communiqué sur l'antisémitisme
================================

Deux émissions de Radio Libertaire, œuvre de la Fédération Anarchiste, ont
été déprogrammées récemment pour non respect de ses principes de base, leurs
animateurs ayant publié un dessin et un visuel à caractère antisémite. Cette
décision a été prise après plusieurs tentatives de concertation qui n'ont
pu aboutir. Par bassesse, pour trouver des soutiens, certains essaient de faire
croire que ces émissions ont été déprogrammées du fait qu'elles parlaient
de la Palestine, or il n'en est rien. Plus grave, des personnes en viennent à
menacer certains mandatés de la radio ainsi que des animateurs membres ou non
de la Fédération Anarchiste jusque dans leurs réseaux professionnels. Nous
ne pouvons accepter cela, nous condamnons ces intimidations et ces menaces et
nous apportons tout notre soutien à Radio Libertaire ainsi qu'à ces mandatés,
qui ont pris une décision juste et aux animateurs qui sont à leurs côtés. On
ne peut tolérer que par le biais de notre radio une idéologie antisémite soit
propagée. Nous avons toujours combattu l’antisémitisme et cela continuera.
