
.. index::
   pair: FA; IFA 
   
   
.. _ifa:
   
===================================================================================
Internationale des fédérations anarchistes 
===================================================================================

.. seealso::

   - https://www.federation-anarchiste.org/?g=IFA_federations
   - http://i-f-a.org



.. toctree::
   :maxdepth: 3
   
   contact/contact
   fa_italie/fa_italie
   
