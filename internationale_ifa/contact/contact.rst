
.. index::
   pair: Contact; IFA 
   
   
.. _ifa_contcat:
   
===================================================================================
Contact IFA
===================================================================================

.. seealso::

   - https://www.federation-anarchiste.org/?g=IFA_federations
   - http://i-f-a.org



Contact : 	secretariat@i-f-a.org
Site web : 	http://i-f-a.org
	
L‘IFA est une organisation internationale de fédérations anarchistes qui se 
rattache, par son pacte associatif et son action, aux principes de la premiere 
Internationale anarchiste qui s‘est constituée a Saint-Imier en 1872.

L‘IFA lutte pour:

- Abolir toute forme d‘autorité qu‘elle soit économique, politique, sociale, 
  religieuse, culturelle ou sexuelle.
- Construire une société libre, sans classes ni Etats ni frontières, fondée 
  sur le fédéralisme libertaire et l‘entraide.

L ‘action de l‘IFA se basera toujours, tant au plan pratique que théorique, 
sur l‘action directe, contre le parlementarisme et le réformisme, 

Les fédérations, adhérentes à l‘IFA, s‘engagent à développer entre elles la 
solidarité la plus efficace dans tous les domaines ; à coopérer et coordonner 
toute initiative ; à fournir une aide régulière à l‘IFA et à son secretariat ; 
à developper, à l‘échelle mondiale, l‘action anarchiste. 

Chaque fédération étant autonome dans sa propagande et son développement.

