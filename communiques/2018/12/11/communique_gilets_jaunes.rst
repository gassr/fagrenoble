

==========================
Communiqué Gilets jaunes
==========================

.. seealso::

   - http://www.federation-anarchiste.org/
   - ifa@federation-anarchiste.org


Un mouvement de colère comme on n'en a pas vu depuis longtemps en France est
apparu il y a plusieurs semaines, désorientant totalement le pouvoir étatique.

Il est compréhensible et légitime face à la violence sociale subie; la diversité
de ses acteurs est le symptôme du degré de ras-le-bol auquel ont mené les
politiques successives d'austérité plus ou moins maquillée.

Cette révolte, même si elle est spectaculaire, ne fait que s'ajouter aux
derniers mouvements sociaux en cours ici et là : hôpitaux, SNCF, facs, lycées,
etc.

La principale caractéristique de ce mouvement est son rejet de la représentation
politicienne et a fortiori des leaders autoproclamés.

Sur les ronds-points occupés s'inventent de nouveaux modes de socialisation.

Les anarchistes ne peuvent qu'approuver une attitude qui est la leur depuis
toujours. Cependant, la révolte est vaine si elle n'est suivie de propositions.
Celles-ci existent, comme par exemple à Saint-Nazaire ou Commercy, et même si
elles ne satisfont pas pleinement des anarchistes, elles méritent d'être
soutenues tant qu'elles vont dans le sens de l'émancipation.

Nous tenons à dénoncer et condamner les très nombreuses violences répressives
de l'État (arrestations arbitraires, comparutions immédiates, mutilations dues
aux tirs de flash-ball et d'autres armes offensives policières, etc) comme
seules réponses aux contestations.

La victoire de ce mouvement ne sera pas la dissolution de l'Assemblée nationale
(surtout pour y installer des démagogues/populistes et/ou nationalistes qui
eux-mêmes ne tolèreraient pas de telles manifestations) mais sa mise hors
d'état de nuire par l'instauration de l'autogestion et du fédéralisme libertaire.


Fédération Anarchiste
