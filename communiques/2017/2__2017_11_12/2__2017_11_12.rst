
.. index::
   pair: Communiqués ; 2017-11-12



.. _communiques_FA_2017_11_12:

==========================================================
Communiqué du 12 novembre 2017 de la Fédération Anarchiste
==========================================================


:download:`Télécharger le communiqué au format PDF <communique_76e_congres_2017_11_12.pdf>`


.. seealso::

   - http://www.federation-anarchiste.org/
   
   

La Fédération anarchiste, réunie en son 76e congrès à Paris 
les 11 et 12 novembre 2017, a débattu de la situation politique et 
sociale et des enjeux de la lutte des classes.

L’État «macroniste» se radicalise et accélère le programme capitaliste 
mené par Gattaz, l’OCDE, le FMI et consorts, dans la continuité des 
gouvernements précédents.

Le capitalisme ne connaît pas de frontières et la lutte pour 
l’émancipation doit également s’affranchir des États et étendre le 
combat révolutionnaire à l’échelle mondiale. 
C’est pourquoi nous développons l’Internationale des Fédérations anarchistes.

Le mouvement social et syndical peine à construire un rapport de forces 
et la convergence des luttes.

La stratégie de mobilisation dans la rue ne remplace pas le blocage économique. 
Seule la grève générale reconductible peut menacer les profits et ouvrir 
ainsi une perspective révolutionnaire.
La Fédération anarchiste est engagée dans ce mouvement social et syndical 
qui doit assumer et affirmer sa légitimité politique et opposer un 
projet de société en rupture avec le modèle capitaliste et du chacun-pour-soi 
qu’on nous impose.

La Fédération anarchiste participera de toutes ses forces au mouvement 
social et s’opposera à toute tentative de tutelle politique y compris 
celle de Mélenchon qui crée la division en portant la lutte sur le 
terrain parlementaire.

Notre rôle est de préserver le mouvement social de toute contamination 
politicienne et électorale.

La Charte d’Amiens demeure pertinente aujourd’hui ; malgré ses 
imperfections et ses limites, elle reste à nos yeux un outil nécessaire 
affirmant les principes de l’action syndicale :

- L’amélioration continue et immédiate des conditions de travail et la 
  transformation sociale ;
- L’indépendance par rapport aux partis et à l’État ;
- La construction de la grève générale expropriatrice et autogestionnaire 
  qui abolira le salariat.
  
Notre mouvement doit également diffuser des pratiques d’auto-organisation 
et d’action directe. La propagande par l’exemple et les alternatives en 
actes sont à même de développer des pratiques en rupture avec le 
consumérisme et la délégation.

Les religions ont toujours été l’outil des puissants, du patriarcat et 
du militarisme. Au côté du Capital, elles incitent à la résignation et 
à l’ignorance. La Fédération anarchiste revendique haut et fort 
**Ni dieu ni maître**. L’émancipation de l’humanité nécessite une lutte 
radicale et définitive avec l’idée de dieu.

La Fédération anarchiste appelle les individus et les groupes attachés 
à l’émancipation sociale à s’organiser et à oeuvrer pour une société 
fédéraliste libertaire et autogestionnaire, pour un monde débarrassé 
des classes, des religions et des États et toutes les dominations.


Paris, le 12 novembre 2017


