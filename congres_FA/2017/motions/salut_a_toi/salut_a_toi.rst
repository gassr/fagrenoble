

.. index::
   pair: Motion ; Salut à Toi (2017)



.. _salut_a_toi:

========================================================================
Motion Communiqué du congrès **Salut à Toi**
========================================================================


.. seealso::

   - https://grainedanar.org/2017/06/12/motion-salut-a-toi-75-eme-congres-de-la-fa/



Réunie à son 75ème congrès, à Merlieux (Aisne), la Fédération anarchiste 
salue toutes celles et tous ceux qui luttent, individuellement et 
collectivement, ou qui construisent des alternatives, notamment: 

- dans les ZAD, 
- contre les projets inutiles imposés, 
- contre le nucléaire et les énergies fossiles, 
- pour les droits des travailleuses et travailleurs, 
- pour les droits des chômeurs et chômeuses, 
- pour une agriculture paysanne, 
- contre le racisme, 
- contre le sexisme, le patriarcat, la haine envers les LGBTI 
  (lesbiennes, gays, bi-sexuel-le-s, trans’, intersexes), 
- pour l’accueil des migrant-e-s, 
- contre la répression... 

Pour la justice sociale, la liberté, l’égalité économique et sociale, 
la Fédération anarchiste, selon ses moyens, sera toujours à vos côtés. 

Rennes, le 5 juin 2017 

