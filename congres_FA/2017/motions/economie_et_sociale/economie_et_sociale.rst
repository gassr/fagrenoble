

.. index::
   pair: Motion ; Economie et sociale (2017)



.. _economie_et_sociale_2017:

========================================================================
Motion sur la situation économique et sociale (2017)
========================================================================



La Fédération anarchiste s'est réunie lors de son 75e Congrès les 3, 4 
et 5 juin 2017 à Laon et merlieux dans l'Aisne pour aborder l'évolution 
du monde et actualiser son projet anarchiste.

Le capitalisme globalisé et ses soutiens étatiques et religieux accentuent 
leurs pressions sur l'humanité et l'environnement, pour asseoir par 
des modes de régulation et de production de plus en plus brutaux le 
triomphe de la logique de profit.

La thèse de la crise permanente du capitalisme qui justifie les politiques 
d'austérité et de régression sociale sert à camoufler la réalité: les 
riches sont de plus en plus riches tandis que les pauvres sont de plus 
en plus nombreux et plongent dans la misère. En cela, le capitalisme 
remplit parfaitement le rôle qui est le sien.

L'arrogance des capitalistes encouragée par une véritable colonisation des esprits remet aujourd'hui
en cause le pacte concédé à l'issue de la seconde guerre mondiale qui institua la protection sociale,
le système de retraite par répartition et une redistribution limitée des richesses pour éloigner le
spectre révolutionnaire et développer une nouvelle classe dite moyenne et la société industrielle et
de consommation de masse.

Le système capitaliste cherche sans cesse de nouvelles sources de profits incarnées aujourd'hui par
le capitalisme vert, la numérisation et la robotisation de l'économie et même la fin du salariat en
individualisant la relation entre le travailleur et son patron par l'uberisation et l'auto-entreprenariat.

Le capitalisme paupérise la classe moyenne dont la première utilité était de neutraliser le danger
révolutionnaire, ce qui rend la lutte des classes plus franche et plus directe.
En contrepartie, l'appareil répressif étatique et l'emprise des religions sur les consciences se
renforcent pour imposer la peur et la résignation. L'état d'urgence qui était une exception devient la
norme, la présence militaire dans les rues, la surveillance généralisée et les restrictions des libertés
publiques se banalisent, la police se militarise et se radicalise, devient factieuse et multiplie les
violences et les provocations.

Les élections présidentielles passées et législatives à venir démontrent une progression des
tendances souverainiste et populiste en même temps qu'une volonté avérée d'effacer la conscience
de classe en affirmant supprimer le clivage gauche-droite comme l'ont souligné les deux finalistes
de la dernière course à l'Elysée. Le pouvoir a mis en place le candidat le plus à même de défendre
les intérêts du patronat et de la Finance. Sa mission est un programme de démolition sociale dans la
droite ligne des présidents et gouvernements précédents en marche vers le détricotage du Code du
travail, une fiscalité antisociale, la suppression des régimes spéciaux et la mise en place d'un
système de retraites par points...

Le Front national a dépassé les dix millions de voix en jouant sur la peur de l'autre et en s'érigeant
en défenseur de façade des acquis sociaux. Pour autant, le Capital n'a pas aujourd'hui besoin du FN
pour arriver à ses fins: le MEDEF a appelé à voter Macron.

Le projet Mélenchonniste est une impasse. Le modèle du leader charismatique, nouveau sauveur
suprême, messie des temps modernes, incarnation du «peuple» a détourné la question sociale du
terrain de la lutte des classes vers celui de la lutte des places. L'imposture des partis Syriza et
Podemos en Europe ou des régimes de Chavez et Maduro au Venezuela témoignent de leurs
soumissions aux diktats capitalistes: le Pouvoir reste maudit.

Si les anarchistes sont volontairement absents des urnes et du spectacle médiatique, ils demeurent
présents et actifs dans les luttes et les alternatives. Notre anti-électoralisme est en phase avec le rejet
grandissant de la classe politique et un intérêt croissant pour le mandatement impératif, la rotation et
la révocabilité des mandatés et le fédéralisme libertaire.

La tâche des révolutionnaires est de dessiner des perspectives permettant de crédibiliser et
d'envisager la transformation sociale. En cela le mouvement anarchiste n'a pas capitulé et reste
fidèle à son projet révolutionnaire: le socialisme libertaire sans frontières.

Pour cela, la diffusion de nos idées et de nos pratiques doit rester notre tâche essentielle, tout en
apprenant des luttes diverses et en menant le nécessaire travail de remise en cause et de
réactualisation de nos propositions et de nos pratiques.

Les temps prochains nécessiteront la mobilisation de toutes les forces attachées à l'émancipation, la
Fédération anarchiste réunit en son 75e Congrès prendra toute la place qui lui revient dans ce
combat et appelle les individus et groupes qui partagent notre projet commun à la rejoindre et à la
renforcer.
